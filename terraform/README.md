# Terraform IAM Module

Specify the resource prefix value in terraform.tfvars file.
eg. iam_pref = "test" will create test-user, test-policy, test-group and test-role

Reference:
https://blog.container-solutions.com/how-to-create-cross-account-user-roles-for-aws-with-terraform

### Output
```
$ terraform apply -var-file=config/terraform.tfvars

Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.iam.aws_iam_group.this will be created
  + resource "aws_iam_group" "this" {
      + arn       = (known after apply)
      + id        = (known after apply)
      + name      = "test-group"
      + path      = "/"
      + unique_id = (known after apply)
    }

  # module.iam.aws_iam_group_policy_attachment.this will be created
  + resource "aws_iam_group_policy_attachment" "this" {
      + group      = "test-group"
      + id         = (known after apply)
      + policy_arn = (known after apply)
    }

  # module.iam.aws_iam_policy.this will be created
  + resource "aws_iam_policy" "this" {
      + arn         = (known after apply)
      + description = "allow assuming role"
      + id          = (known after apply)
      + name        = "test-policy"
      + path        = "/"
      + policy      = (known after apply)
      + policy_id   = (known after apply)
      + tags_all    = (known after apply)
    }

  # module.iam.aws_iam_role.this will be created
  + resource "aws_iam_role" "this" {
      + arn                   = (known after apply)
      + assume_role_policy    = jsonencode(
            {
              + Statement = [
                  + {
                      + Action    = "sts:AssumeRole"
                      + Effect    = "Allow"
                      + Principal = {
                          + AWS = "arn:aws:iam::543731983714:root"
                        }
                    },
                ]
              + Version   = "2012-10-17"
            }
        )
      + create_date           = (known after apply)
      + force_detach_policies = false
      + id                    = (known after apply)
      + managed_policy_arns   = (known after apply)
      + max_session_duration  = 3600
      + name                  = "test-role"
      + name_prefix           = (known after apply)
      + path                  = "/"
      + tags_all              = (known after apply)
      + unique_id             = (known after apply)

      + inline_policy {
          + name   = (known after apply)
          + policy = (known after apply)
        }
    }

  # module.iam.aws_iam_user.this will be created
  + resource "aws_iam_user" "this" {
      + arn           = (known after apply)
      + force_destroy = false
      + id            = (known after apply)
      + name          = "test-user"
      + path          = "/"
      + tags_all      = (known after apply)
      + unique_id     = (known after apply)
    }

  # module.iam.aws_iam_user_group_membership.this will be created
  + resource "aws_iam_user_group_membership" "this" {
      + groups = [
          + "test-group",
        ]
      + id     = (known after apply)
      + user   = "test-user"
    }

Plan: 6 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.iam.aws_iam_group.this: Creating...
module.iam.aws_iam_user.this: Creating...
module.iam.aws_iam_role.this: Creating...
module.iam.aws_iam_user.this: Creation complete after 2s [id=test-user]
module.iam.aws_iam_group.this: Creation complete after 2s [id=test-group]
module.iam.aws_iam_user_group_membership.this: Creating...
module.iam.aws_iam_user_group_membership.this: Creation complete after 2s [id=terraform-20211228132917003200000001]
module.iam.aws_iam_role.this: Creation complete after 5s [id=test-role]
module.iam.aws_iam_policy.this: Creating...
module.iam.aws_iam_policy.this: Creation complete after 3s [id=arn:aws:iam::543731983714:policy/test-policy]
module.iam.aws_iam_group_policy_attachment.this: Creating...
module.iam.aws_iam_group_policy_attachment.this: Creation complete after 2s [id=test-group-20211228132922596100000002]

Apply complete! Resources: 6 added, 0 changed, 0 destroyed.
```
