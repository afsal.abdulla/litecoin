# Text Manipulation
### Consider an apache log file (eg. access.log), print all unique HTTP response codes in the log file and their count in the following format

```
200 10
301 5
302 7
```

### Bash
```
$ awk '{ print $10 }' access.log  | sed -r '/^\s*$/d' | sort | uniq -c | awk ' { t = $1; $1 = $2; $2 = t; print; }'
200 39
302 1
304 2
404 5
502 3
```

### Python
```
$ python3 text.py 
200 39
302 1
304 2
404 5
502 3
```
