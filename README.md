# Litecoin 0.18.1



## Prerequisites

* Docker - Configured in Jenkins server
* minikube - Configured in Jenkins server


## Jenkins Pipeline

### Parmeters
* Repo - Repository URL
* Branch - Repository Branch
* Litecoin_Version - Litecoin version to build and deploy


### Files
* litecoin.Dockerfile - Dockerfile for building litecoin image
* litecoin.Jenkinsfile - Jenkins pipeline code
* litecoin.yaml - k8s stateful set (ref. https://kubernetes.io/docs/tutorials/stateful-application/basic-stateful-set/)

#### Anchore inline scanning is used for scanning vulnerabilities of the Docker image

### Output
```
Started by user Afsal Abdulla
[Pipeline] Start of Pipeline
[Pipeline] node
Running on Jenkins in /var/lib/jenkins/workspace/litecoin
[Pipeline] {
[Pipeline] ansiColor
[Pipeline] {

[Pipeline] stage
[Pipeline] { (Initialize Repo)
[Pipeline] cleanWs
[WS-CLEANUP] Deleting project workspace...
[WS-CLEANUP] Deferred wipeout is used...
[WS-CLEANUP] done
[Pipeline] git
The recommended git tool is: NONE
No credentials specified
Cloning the remote Git repository
Cloning repository https://gitlab.com/afsal.abdulla/litecoin.git
 > git init /var/lib/jenkins/workspace/litecoin # timeout=10
Fetching upstream changes from https://gitlab.com/afsal.abdulla/litecoin.git
 > git --version # timeout=10
 > git --version # 'git version 2.32.0'
 > git fetch --tags --force --progress -- https://gitlab.com/afsal.abdulla/litecoin.git +refs/heads/*:refs/remotes/origin/* # timeout=10
 > git config remote.origin.url https://gitlab.com/afsal.abdulla/litecoin.git # timeout=10
 > git config --add remote.origin.fetch +refs/heads/*:refs/remotes/origin/* # timeout=10
Avoid second fetch
 > git rev-parse refs/remotes/origin/main^{commit} # timeout=10
Checking out Revision b448360885bfc157d6ad4d381c79e36e1ac34560 (refs/remotes/origin/main)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f b448360885bfc157d6ad4d381c79e36e1ac34560 # timeout=10
 > git branch -a -v --no-abbrev # timeout=10
 > git checkout -b main b448360885bfc157d6ad4d381c79e36e1ac34560 # timeout=10
Commit message: "litecoin.Jenkinsfile"
 > git rev-list --no-walk 6fff0e411685618fb821a00874bffaeea6876395 # timeout=10
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Build)
[Pipeline] sh
+ docker build -f litecoin.Dockerfile -t litecoin:0.18.1 --build-arg VERSION=0.18.1 .
Sending build context to Docker daemon   93.7kB

Step 1/13 : ARG VERSION=0.18.1
Step 2/13 : FROM alpine:latest as base
latest: Pulling from library/alpine
59bf1c3509f3: Pulling fs layer
59bf1c3509f3: Verifying Checksum
59bf1c3509f3: Download complete
59bf1c3509f3: Pull complete
Digest: sha256:21a3deaa0d32a8057914f36584b5288d2e5ecc984380bc0118285c70fa8c9300
Status: Downloaded newer image for alpine:latest
 ---> c059bfaa849c
Step 3/13 : ARG VERSION
 ---> Running in 81e5336a83cb
Removing intermediate container 81e5336a83cb
 ---> 06a768df557c
Step 4/13 : ARG ARCH=x86_64
 ---> Running in d00407b0f84a
Removing intermediate container d00407b0f84a
 ---> d2044537846d
Step 5/13 : RUN apk update && apk --no-cache add curl && curl -fsSL     "https://download.litecoin.org/litecoin-${VERSION}/linux/litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz"     -o "/tmp/litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz"     && SHA=`echo $(curl -fsSL     "https://download.litecoin.org/litecoin-${VERSION}/linux/litecoin-${VERSION}-linux-signatures.asc" |     grep "litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz" | awk '{print $1}')`     && echo "$SHA  /tmp/litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz" | sha256sum -c -     && tar -xzvf "/tmp/litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz" -C /tmp/
 ---> Running in 92309d915120
fetch https://dl-cdn.alpinelinux.org/alpine/v3.15/main/x86_64/APKINDEX.tar.gz
fetch https://dl-cdn.alpinelinux.org/alpine/v3.15/community/x86_64/APKINDEX.tar.gz
v3.15.0-145-g123e3b9a98 [https://dl-cdn.alpinelinux.org/alpine/v3.15/main]
v3.15.0-153-ga8b507f090 [https://dl-cdn.alpinelinux.org/alpine/v3.15/community]
OK: 15838 distinct packages available
fetch https://dl-cdn.alpinelinux.org/alpine/v3.15/main/x86_64/APKINDEX.tar.gz
fetch https://dl-cdn.alpinelinux.org/alpine/v3.15/community/x86_64/APKINDEX.tar.gz
(1/5) Installing ca-certificates (20191127-r7)
(2/5) Installing brotli-libs (1.0.9-r5)
(3/5) Installing nghttp2-libs (1.46.0-r0)
(4/5) Installing libcurl (7.80.0-r0)
(5/5) Installing curl (7.80.0-r0)
Executing busybox-1.34.1-r3.trigger
Executing ca-certificates-20191127-r7.trigger
OK: 8 MiB in 19 packages
/tmp/litecoin-0.18.1-x86_64-linux-gnu.tar.gz: OK
litecoin-0.18.1/
litecoin-0.18.1/bin/
litecoin-0.18.1/bin/litecoin-cli
litecoin-0.18.1/bin/litecoind
litecoin-0.18.1/bin/litecoin-qt
litecoin-0.18.1/bin/litecoin-tx
litecoin-0.18.1/bin/litecoin-wallet
litecoin-0.18.1/bin/test_litecoin
litecoin-0.18.1/README.md
litecoin-0.18.1/share/
litecoin-0.18.1/share/man/
litecoin-0.18.1/share/man/man1/
litecoin-0.18.1/share/man/man1/litecoin-cli.1
litecoin-0.18.1/share/man/man1/litecoind.1
litecoin-0.18.1/share/man/man1/litecoin-qt.1
litecoin-0.18.1/share/man/man1/litecoin-tx.1
litecoin-0.18.1/share/man/man1/litecoin-wallet.1
Removing intermediate container 92309d915120
 ---> 77c4fe06f39a
Step 6/13 : FROM debian:bullseye-slim
bullseye-slim: Pulling from library/debian
a2abf6c4d29d: Pulling fs layer
a2abf6c4d29d: Verifying Checksum
a2abf6c4d29d: Download complete
a2abf6c4d29d: Pull complete
Digest: sha256:b0d53c872fd640c2af2608ba1e693cfc7dedea30abcd8f584b23d583ec6dadc7
Status: Downloaded newer image for debian:bullseye-slim
 ---> 8bca477113bd
Step 7/13 : ARG VERSION
 ---> Running in b69b108ab6bf
Removing intermediate container b69b108ab6bf
 ---> d9bdeb39f4cc
Step 8/13 : RUN useradd litecoin
 ---> Running in 4248fa704f2b
Removing intermediate container 4248fa704f2b
 ---> 73e31a12dba1
Step 9/13 : EXPOSE 9332 9333
 ---> Running in bf13aaa28773
Removing intermediate container bf13aaa28773
 ---> 15c8b5bd84ce
Step 10/13 : VOLUME ["/home/litecoin/.litecoin"]
 ---> Running in bb1600ce38a1
Removing intermediate container bb1600ce38a1
 ---> 950ffa7ec68a
Step 11/13 : COPY --from=base --chown=litecoin:litecoin /tmp/litecoin-${VERSION}/ /home/litecoin/
 ---> 27c943d4dbab
Step 12/13 : USER litecoin:litecoin
 ---> Running in ced7e39edcf1
Removing intermediate container ced7e39edcf1
 ---> 3b099be9fdad
Step 13/13 : CMD /home/litecoin/bin/litecoind
 ---> Running in e509ea3725a3
Removing intermediate container e509ea3725a3
 ---> 8c75fb6136ad
Successfully built 8c75fb6136ad
Successfully tagged litecoin:0.18.1
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Vulnerability Check)
[Pipeline] sh
+ curl -s https://ci-tools.anchore.io/inline_scan-latest
+ bash -s -- -r litecoin:0.18.1
**WARNING**

The Anchore Inline Scan script is deprecated and will reach EOL on Jan 10, 2022.
The vulnerability database will no longer be kept up to date and new images will no longer be published.

Please update your integrations to use our new CLI vulnerability scanning tool, grype.

https://github.com/anchore/grype

**WARNING**

Pulling docker.io/anchore/inline-scan:v0.10.2
v0.10.2: Pulling from anchore/inline-scan
Digest: sha256:34a7237011f05a04eea39bdfa448b951912c29491c213e0591661d9cd588312f
Status: Image is up to date for anchore/inline-scan:v0.10.2
docker.io/anchore/inline-scan:v0.10.2
Starting Anchore Engine
Starting Postgresql... Postgresql started successfully!
Starting Docker registry... Docker registry started successfully!
Waiting for Anchore Engine to be available.

	Status: not_ready...

Anchore Engine is available!


Preparing litecoin:0.18.1 for analysis

Getting image source signatures
Copying blob sha256:6917c5bd4fab35fe7a732f4aa0721d22c2b16f132e9cd872d3dfdc12b7c58e2d
Copying blob sha256:2edcec3590a4ec7f40cf0743c15d78fb39d8326bc029073b41ef9727da6c851f
Copying blob sha256:c1ea7db26b98f37a3587d7d39ea8df223b347a4f8ee9e53943e738e5b01fb65c
Copying config sha256:8c75fb6136ad33fb2cac68499277f62e1fc04e3c345ac253029a4caa1f73421a
Writing manifest to image destination
Storing signatures

Image archive loaded into Anchore Engine using tag -- litecoin:0.18.1
Waiting for analysis to complete...

	Status: not_analyzed.
	Status: analyzing..........
	Status: analyzed

Analysis completed!

Successfully generated anchore-reports/litecoin_0.18.1-content-os.json.
Successfully generated anchore-reports/litecoin_0.18.1-content-files.json.
Successfully generated anchore-reports/litecoin_0.18.1-vuln.json.
Successfully generated anchore-reports/litecoin_0.18.1-details.json.
Successfully generated anchore-reports/litecoin_0.18.1-policy.json.

	Policy Evaluation - litecoin:0.18.1
-----------------------------------------------------------

Image Digest: sha256:8e905c7020287c06b131911586adccf6a9a48866cb75a53f7e455c2c10470d4f
Full Tag: localhost:5000/litecoin:0.18.1
Image ID: 8c75fb6136ad33fb2cac68499277f62e1fc04e3c345ac253029a4caa1f73421a
Status: pass
Last Eval: 2021-12-28T11:49:04Z
Policy ID: 2c53a13c-1765-11e8-82ef-23527761d060
Final Action: warn
Final Action Reason: policy_evaluation

Gate                   Trigger            Detail                                                                                                                                                Status        
dockerfile             instruction        Dockerfile directive 'HEALTHCHECK' not found, matching condition 'not_exists' check                                                                   warn          
vulnerabilities        package            MEDIUM Vulnerability found in os package type (dpkg) - libgnutls30 (CVE-2011-3389 - https://security-tracker.debian.org/tracker/CVE-2011-3389)        warn          


**WARNING**

The Anchore Inline Scan script is deprecated and will reach EOL on Jan 10, 2022.
The vulnerability database will no longer be kept up to date and new images will no longer be published.

Please update your integrations to use our new CLI vulnerability scanning tool, grype.

https://github.com/anchore/grype

**WARNING**

Copying scan reports from 5391-inline-anchore-engine to /var/lib/jenkins/workspace/litecoin/anchore-reports/

Cleaning up docker container: 5391-inline-anchore-engine
[Pipeline] }
[Pipeline] // stage
[Pipeline] stage
[Pipeline] { (Deploy)
[Pipeline] input
Deploy?
Deploy or Abort
Approved by Afsal Abdulla
[Pipeline] sh
+ /usr/local/bin/minikube status
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured

[Pipeline] sh
+ /usr/local/bin/kubectl apply -f litecoin.yaml
namespace/litecoin created
service/litecoin created
statefulset.apps/litecoin created
[Pipeline] cleanWs
[WS-CLEANUP] Deleting project workspace...
[WS-CLEANUP] Deferred wipeout is used...
[WS-CLEANUP] done
[Pipeline] }
[Pipeline] // stage
[Pipeline] }

[Pipeline] // ansiColor
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
```

### Additional Outputs
```
$ kubectl --namespace litecoin get statefulset
NAME       READY   AGE
litecoin   1/1     13m

$ kubectl --namespace litecoin describe pods litecoin-0
Name:         litecoin-0
Namespace:    litecoin
Priority:     0
Node:         ip-10-0-0-59.ap-south-1.compute.internal/10.0.0.59
Start Time:   Tue, 28 Dec 2021 11:56:09 +0000
Labels:       app=litecoin
              controller-revision-hash=litecoin-54dd985f4f
              statefulset.kubernetes.io/pod-name=litecoin-0
Annotations:  <none>
Status:       Running
IP:           172.17.0.3
IPs:
  IP:           172.17.0.3
Controlled By:  StatefulSet/litecoin
Containers:
  litecoin:
    Container ID:   docker://23e5ed2da21338249c26c6122c3035cde1d604841a326650b199c6f1c07979df
    Image:          afsal137/litecoin:latest
    Image ID:       docker-pullable://afsal137/litecoin@sha256:b5c579a3de124e3e8d3cfb0405d8a1ac35d39c0d41380ba42d18f89755096c3f
    Ports:          9332/TCP, 9333/TCP
    Host Ports:     0/TCP, 0/TCP
    State:          Running
      Started:      Tue, 28 Dec 2021 11:56:15 +0000
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     1
      memory:  1Gi
    Requests:
      cpu:        1
      memory:     1Gi
    Environment:  <none>
    Mounts:
      /home/litecoin/.litecoin from litecoin (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-l8wb8 (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  litecoin:
    Type:       PersistentVolumeClaim (a reference to a PersistentVolumeClaim in the same namespace)
    ClaimName:  litecoin-litecoin-0
    ReadOnly:   false
  kube-api-access-l8wb8:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   Guaranteed
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type     Reason            Age   From               Message
  ----     ------            ----  ----               -------
  Warning  FailedScheduling  14m   default-scheduler  0/1 nodes are available: 1 pod has unbound immediate PersistentVolumeClaims.
  Normal   Scheduled         14m   default-scheduler  Successfully assigned litecoin/litecoin-0 to ip-10-0-0-59.ap-south-1.compute.internal
  Normal   Pulling           14m   kubelet            Pulling image "afsal137/litecoin:latest"
  Normal   Pulled            14m   kubelet            Successfully pulled image "afsal137/litecoin:latest" in 5.168491714s
  Normal   Created           14m   kubelet            Created container litecoin
  Normal   Started           14m   kubelet            Started container litecoin
```

# Text Manipulation
### Consider an apache log file (eg. access.log), print all unique HTTP response codes in the log file and their count in the following format

```
200 10
301 5
302 7
```

### Bash
```
$ awk '{ print $10 }' access.log  | sed -r '/^\s*$/d' | sort | uniq -c | awk ' { t = $1; $1 = $2; $2 = t; print; }'
200 39
302 1
304 2
404 5
502 3
```

### Python
```
$ python3 text.py 
200 39
302 1
304 2
404 5
502 3
```
