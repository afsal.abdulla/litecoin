#Multistage Buld Args
ARG VERSION=0.18.1

FROM alpine:latest as base

#Build Args
ARG VERSION
ARG ARCH=x86_64

#Install Curl, Download Package, Verify Checksum and Extract Package
RUN apk update && apk --no-cache add curl && curl -fsSL \
    "https://download.litecoin.org/litecoin-${VERSION}/linux/litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz" \
    -o "/tmp/litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz" \
    && SHA=`echo $(curl -fsSL \
    "https://download.litecoin.org/litecoin-${VERSION}/linux/litecoin-${VERSION}-linux-signatures.asc" | \
    grep "litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz" | awk '{print $1}')` \
    && echo "$SHA  /tmp/litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz" | sha256sum -c - \
    && tar -xzvf "/tmp/litecoin-${VERSION}-${ARCH}-linux-gnu.tar.gz" -C /tmp/
 

FROM debian:bullseye-slim
#Build Args
ARG VERSION


RUN useradd litecoin

#Service Port 9333, RPC Port 9332 
EXPOSE 9332 9333
VOLUME ["/home/litecoin/.litecoin"]

#Copy litecoin bin from base image
COPY --from=base --chown=litecoin:litecoin /tmp/litecoin-${VERSION}/ /home/litecoin/
USER litecoin:litecoin
CMD /home/litecoin/bin/litecoind
