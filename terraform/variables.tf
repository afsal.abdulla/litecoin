variable "iam_pref" {
  description   = "Prefix for the IAM role, group, policy and user"
  type          = string
  default       = "iam"
}