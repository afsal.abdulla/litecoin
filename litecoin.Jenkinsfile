def Repo = "https://gitlab.com/afsal.abdulla/litecoin.git"

pipeline {
    agent any
        parameters {
          string(name: 'Branch', defaultValue: 'main', description: 'Branch to build.')
          string(name: 'Litecoin_Version', defaultValue: '0.18.1', description: 'Litecoin Version to deploy')
        }
    options {
        ansiColor('xterm')
    }

    stages {
        stage('Initialize Repo'){
            steps {
                cleanWs()
                git url: Repo, branch: params.Branch
            }
        }
        stage('Build') {
            steps {
                sh "docker build -f litecoin.Dockerfile -t afsal137/litecoin:${params.Litecoin_Version} --build-arg VERSION=${params.Litecoin_Version} ."
            }
        }
        stage('Vulnerability Check') {
            steps {
                sh "curl -s https://ci-tools.anchore.io/inline_scan-latest | bash -s -- -r afsal137/litecoin:${params.Litecoin_Version}"
            }
        }
        stage('Deploy') {
            input {
                message "Deploy?"
                ok "Deploy"
            }
            steps {
                sh "docker tag afsal137/litecoin:${params.Litecoin_Version} afsal137/litecoin:latest"
                sh "docker push afsal137/litecoin:${params.Litecoin_Version}; docker push afsal137/litecoin:latest"
                sh "/usr/local/bin/minikube status || /usr/local/bin/minikube start --vm-driver=none"
                sh "/usr/local/bin/kubectl apply -f litecoin.yaml"
                cleanWs()
            }
        }
    }      
}
